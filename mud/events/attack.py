from .event import Event3

class AttackWithEvent(Event3):
    NAME = "attack"

    def perform(self):
        self.inform("attack-with")


